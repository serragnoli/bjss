package com.bjss.pb.core.model.promotion;

import com.bjss.pb.core.model.product.Unit;

import java.math.BigDecimal;

public class AppliedPromotionFactory {
    public static AppliedPromotion appliedPromotionTo(final String name, final String discountableProductId, final BigDecimal amount) {
        final String formattedName = String.format(name, discountableProductId);

        return AppliedPromotion.builder()
                .name(formattedName)
                .discountableProductId(discountableProductId)
                .discount(amount)
                .build();
    }

    public static AppliedPromotion appliedPromotionTo(final String name, final Unit triggerUnit,
                                                      final String triggerProdId, final String discountableProdId,
                                                      final BigDecimal discount) {
        final String formattedName = String.format(name, triggerUnit.getDisplay(), triggerProdId, discountableProdId);

        return AppliedPromotion.builder()
                .name(formattedName)
                .unit(triggerUnit.getDisplay())
                .triggerProductId(triggerProdId)
                .discountableProductId(discountableProdId)
                .discount(discount)
                .build();
    }
}
