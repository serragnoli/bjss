package com.bjss.pb.core.model.order;

import com.bjss.pb.core.model.product.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.bjss.pb.core.model.product.ProductFactory.BREAD;
import static com.bjss.pb.core.model.product.ProductFactory.MILK;
import static com.bjss.pb.core.model.product.ProductFactory.SOUP;
import static java.math.BigDecimal.valueOf;

public class OrderLinesFactory {

    public static List<OrderLine> linesWithSubTotalOf12() {
        final List<OrderLine> orderLines = new ArrayList<>();

        orderLines.add(new OrderLine(MILK.getId(), valueOf(6), MILK.getUnit(), 1));
        orderLines.add(new OrderLine(SOUP.getId(), valueOf(3), SOUP.getUnit(), 1));
        orderLines.add(new OrderLine(BREAD.getId(), valueOf(3), BREAD.getUnit(), 1));

        return orderLines;
    }

    public static List<OrderLine> linesWith(final Product... products) {
        return Arrays.stream(products)
                .map(p -> new OrderLine(p.getId(), p.getPrice(), p.getUnit(), 1))
                .collect(Collectors.toList());
    }

    public static List<OrderLine> linesWithBreadAnd2TinsOfSoup() {
        final List<OrderLine> orderLines = new ArrayList<>();

        orderLines.add(new OrderLine(MILK.getId(), valueOf(1.3), MILK.getUnit(), 1));
        orderLines.add(new OrderLine(SOUP.getId(), valueOf(0.65), SOUP.getUnit(), 2));
        orderLines.add(new OrderLine(BREAD.getId(), valueOf(0.8), BREAD.getUnit(), 1));

        return orderLines;
    }
}
