package com.bjss.pb.core.actions;

import com.bjss.pb.core.model.basket.BasketService;
import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.order.OrderService;
import com.bjss.pb.core.model.product.Product;
import com.bjss.pb.core.model.product.ProductService;
import com.bjss.pb.core.model.promotion.PromotionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static com.bjss.pb.core.actions.PurchasedProductsBasketFactory.noDiscount;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseProductsTest {

    @Mock
    private BasketService basketService;

    @Mock
    private ProductService productService;

    @Mock
    private OrderService orderService;

    @Mock
    private PromotionService promotionService;

    @InjectMocks
    private PurchaseProducts purchaseProducts;

    @Test
    public void should_invoke_basket_domain_service() {
        purchaseProducts.purchase(noDiscount());

        verify(basketService).normalise(Matchers.<Map<String, List<String>>> any());
    }

    @Test
    public void should_invoke_product_service() {
        purchaseProducts.purchase(noDiscount());

        verify(productService).mapFrom(Matchers.<Map<String, Integer>>any());
    }

    @Test
    public void should_invoke_order_service() {
        purchaseProducts.purchase(noDiscount());

        verify(orderService).createOrderFrom(anyListOf(Product.class), anyMapOf(String.class, Integer.class));
    }

    @Test
    public void should_invoke_promotion_service() {
        purchaseProducts.purchase(noDiscount());

        verify(promotionService).applyPromotionsFor(any(Order.class));
    }
}