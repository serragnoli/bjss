package com.bjss.pb.core.model.basket;

import com.bjss.pb.core.actions.PurchasedProductsBasketFactory;
import com.bjss.pb.core.model.basket.exceptions.NoProductInListException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BasketValidatorTest {

    @InjectMocks
    private BasketValidator validator;

    @Test(expected = NoProductInListException.class)
    public void should_throw_exception_when_products_list_is_null() {
        validator.validate(PurchasedProductsBasketFactory.noProductsList());
    }

    @Test(expected = NoProductInListException.class)
    public void should_throw_exception_when_basket_param_missing_from() {
        validator.validate(PurchasedProductsBasketFactory.missingBasketParam());
    }

    @Test(expected = NoProductInListException.class)
    public void should_throw_exception_when_products_list_is_empty() {
        validator.validate(PurchasedProductsBasketFactory.emptyProductList());
    }

    @Test
    public void should_validate_when_basket_param_contains_list_with_products() {
        validator.validate(PurchasedProductsBasketFactory.noDiscount());
    }
}