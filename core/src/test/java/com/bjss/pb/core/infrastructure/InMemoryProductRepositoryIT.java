package com.bjss.pb.core.infrastructure;

import com.bjss.pb.core.infrastructure.repositories.InMemoryProductRepository;
import com.bjss.pb.core.model.product.Product;
import com.bjss.pb.core.model.product.ProductFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;

import static com.bjss.pb.core.actions.TestCommons.BREAD;
import static com.bjss.pb.core.actions.TestCommons.MILK;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryProductRepositoryIT {

    private static final HashSet<String> BREAND_AND_MILK = new HashSet<>(asList(MILK, BREAD));
    private static final HashSet<String> BREAD_MILK_AND_DODGY = new HashSet<>(asList(MILK, BREAD, "Dodgy"));

    private InMemoryProductRepository inMemoryRepository;

    @Before
    public void setUp() {
        inMemoryRepository = new InMemoryProductRepository();
    }

    @Test
    public void should_fetch_2_products_ids() {
        final List<Product> productsFetched = inMemoryRepository.fetchAll(BREAND_AND_MILK);

        assertThat(productsFetched).hasSize(2);
    }

    @Test
    public void should_fetch_products_with_id_equal_to_passed_ids() {
        final List<Product> productsFetched = inMemoryRepository.fetchAll(BREAND_AND_MILK);

        assertThat(productsFetched).contains(ProductFactory.MILK, ProductFactory.BREAD);
    }

    @Test
    public void should_ignore_unknown_product_ids() {
        final List<Product> productsFetched = inMemoryRepository.fetchAll(BREAD_MILK_AND_DODGY);

        assertThat(productsFetched).hasSize(2);
    }
}