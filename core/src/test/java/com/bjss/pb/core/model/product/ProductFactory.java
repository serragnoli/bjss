package com.bjss.pb.core.model.product;

import java.math.BigDecimal;

public class ProductFactory {
    public static final Product SOUP = new Product("Soup", Unit.TIN, BigDecimal.valueOf(0.65));
    public static final Product BREAD = new Product("Bread", Unit.LOAF, BigDecimal.valueOf(0.8));
    public static final Product MILK = new Product("Milk", Unit.BOTTLE, BigDecimal.valueOf(1.3));
    public static final Product APPLES = new Product("Apples", Unit.BAG, BigDecimal.valueOf(1));

}
