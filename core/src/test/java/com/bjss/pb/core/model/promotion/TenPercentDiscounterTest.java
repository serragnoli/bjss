package com.bjss.pb.core.model.promotion;

import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.order.OrderFactory;
import com.bjss.pb.core.model.product.ProductFactory;
import org.junit.Test;

import java.util.Collections;

import static com.bjss.pb.core.actions.TestCommons.APPLES;
import static com.bjss.pb.core.actions.TestCommons.MILK;
import static com.bjss.pb.core.model.promotion.AppliedPromotionFactory.appliedPromotionTo;
import static com.bjss.pb.core.model.promotion.TenPercentDiscounter.NAME;
import static java.math.BigDecimal.valueOf;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class TenPercentDiscounterTest {

    @Test
    public void should_apply_discount_to_apples() {
        final TenPercentDiscounter discounter = new TenPercentDiscounter(Collections.singletonList(ProductFactory.APPLES));
        final Order order = OrderFactory.milkBreadAndApples();

        discounter.applyDiscount(order);

        assertThat(order.getAppliedPromotions()).contains(appliedPromotionTo(NAME, APPLES, valueOf(0.1)));
    }

    @Test
    public void should_apply_discount_to_multiple_order_lines() {
        final TenPercentDiscounter discounter = new TenPercentDiscounter(asList(ProductFactory.MILK, ProductFactory.APPLES));
        final Order order = OrderFactory.milkBreadAndApples();

        discounter.applyDiscount(order);

        assertThat(order.getAppliedPromotions()).contains(appliedPromotionTo(NAME, APPLES, valueOf(0.1)),
                appliedPromotionTo(NAME, MILK, valueOf(0.13)));
    }
}