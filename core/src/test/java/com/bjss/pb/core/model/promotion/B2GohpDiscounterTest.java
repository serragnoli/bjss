package com.bjss.pb.core.model.promotion;

import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.order.OrderFactory;
import org.junit.Test;

import static com.bjss.pb.core.model.product.ProductFactory.BREAD;
import static com.bjss.pb.core.model.product.ProductFactory.SOUP;
import static com.bjss.pb.core.model.promotion.AppliedPromotionFactory.appliedPromotionTo;
import static com.bjss.pb.core.model.promotion.B2gohpDiscounter.NAME;
import static java.math.BigDecimal.ROUND_HALF_UP;
import static java.math.BigDecimal.valueOf;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;

public class B2GohpDiscounterTest {

    @Test
    public void should_make_bread_half_price() {
        final B2gohpDiscounter discounter = new B2gohpDiscounter(singletonMap(SOUP, BREAD));
        final Order order = OrderFactory.milkBreadAnd2Soups();

        discounter.applyDiscount(order);

        assertThat(order.getAppliedPromotions()).contains(appliedPromotionTo(NAME, SOUP.getUnit(), SOUP.getId(),
                BREAD.getId(), valueOf(0.4).setScale(2, ROUND_HALF_UP)));
    }
}