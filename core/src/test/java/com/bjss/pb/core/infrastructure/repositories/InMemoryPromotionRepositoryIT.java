package com.bjss.pb.core.infrastructure.repositories;

import com.bjss.pb.core.model.promotion.B2gohpDiscounter;
import com.bjss.pb.core.model.promotion.Discounter;
import com.bjss.pb.core.model.promotion.TenPercentDiscounter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.bjss.pb.core.model.product.ProductFactory.APPLES;
import static com.bjss.pb.core.model.product.ProductFactory.BREAD;
import static com.bjss.pb.core.model.product.ProductFactory.SOUP;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class InMemoryPromotionRepositoryIT {

    @InjectMocks
    private InMemoryPromotionRepository repository;

    @Test
    public void should_retrieve_ten_percent_discounter() {
        final List<Discounter> discounters = repository.fetchAllDiscounters();

        assertThat(discounters).contains(new TenPercentDiscounter(Collections.singletonList(APPLES)));
    }

    @Test
    public void should_retrieve_buy_two_a_get_b_half() {
        final List<Discounter> discounters = repository.fetchAllDiscounters();

        assertThat(discounters).contains(new B2gohpDiscounter(Collections.singletonMap(SOUP, BREAD)));
    }
}