package com.bjss.pb.core.model.promotion;

import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.order.OrderFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.singletonList;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AppliedPromotionServiceTest {

    @Mock
    private PromotionRepository repository;

    @Mock
    private Discounter discounter;

    @InjectMocks
    private PromotionService promotionService;

    @Test
    public void should_invoke_repository() {
        promotionService.applyPromotionsFor(OrderFactory.milkAndBread());

        verify(repository).fetchAllDiscounters();
    }

    @Test
    public void should_invoke_apply_promotion() {
        when(repository.fetchAllDiscounters()).thenReturn(singletonList(discounter));

        promotionService.applyPromotionsFor(OrderFactory.milkAndBread());

        verify(discounter).applyDiscount(any(Order.class));
    }
}