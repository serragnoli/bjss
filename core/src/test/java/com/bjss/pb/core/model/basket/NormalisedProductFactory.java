package com.bjss.pb.core.model.basket;

import com.bjss.pb.core.actions.TestCommons;

import java.util.HashMap;
import java.util.Map;

public class NormalisedProductFactory {
    public static Map<String, Integer> milksAndBread() {
        final Map<String, Integer> normalisedProducts = new HashMap<>();

        normalisedProducts.put(TestCommons.MILK, 3);
        normalisedProducts.put(TestCommons.BREAD, 1);

        return normalisedProducts;
    }
}
