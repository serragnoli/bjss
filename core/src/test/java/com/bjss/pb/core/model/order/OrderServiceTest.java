package com.bjss.pb.core.model.order;

import com.bjss.pb.core.model.product.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.bjss.pb.core.model.basket.NormalisedProductFactory.milksAndBread;
import static com.bjss.pb.core.model.product.ProductFactory.BREAD;
import static com.bjss.pb.core.model.product.ProductFactory.MILK;
import static java.util.Arrays.asList;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    private ToOrderLineConverter toOrderLineConverter;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void should_invoke_order_line_converter() {
        orderService.createOrderFrom(asList(BREAD, MILK), milksAndBread());

        verify(toOrderLineConverter).convertFrom(anyListOf(Product.class), anyMapOf(String.class, Integer.class));
    }
}