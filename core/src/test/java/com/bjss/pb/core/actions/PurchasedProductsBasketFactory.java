package com.bjss.pb.core.actions;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.bjss.pb.core.infrastructure.utils.ActionsCommonParams.BASKET_PARAM;
import static com.bjss.pb.core.actions.TestCommons.BREAD;
import static com.bjss.pb.core.actions.TestCommons.MILK;
import static java.util.Arrays.asList;

public class PurchasedProductsBasketFactory {
    public static Map<String, List<String>> noDiscount() {
        return Collections.singletonMap(BASKET_PARAM, asList(MILK, BREAD));
    }

    public static Map<String, List<String>> noProductsList() {
        return Collections.singletonMap(BASKET_PARAM, null);
    }

    public static Map<String, List<String>> missingBasketParam() {
        return Collections.singletonMap(null, asList(MILK, BREAD));
    }

    public static Map<String, List<String>> emptyProductList() {
        return Collections.singletonMap(BASKET_PARAM, Collections.emptyList());
    }
}
