package com.bjss.pb.core.model.basket;

import com.bjss.pb.core.model.order.OrderLine;
import com.bjss.pb.core.model.order.ToOrderLineConverter;
import com.bjss.pb.core.model.product.ProductFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static com.bjss.pb.core.model.basket.NormalisedProductFactory.milksAndBread;
import static com.bjss.pb.core.model.product.ProductFactory.BREAD;
import static com.bjss.pb.core.model.product.ProductFactory.MILK;
import static java.math.BigDecimal.valueOf;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ToOrderLineConverterTest {

    @InjectMocks
    private ToOrderLineConverter toOrderLineConverter;

    @Test
    public void should_convert_products_to_order_lines() {
        final List<OrderLine> orderLines = toOrderLineConverter.convertFrom(asList(MILK, BREAD), milksAndBread());

        assertThat(orderLines).hasSize(2);
    }

    @Test
    public void order_line_for_milk_contains_id() {
        final List<OrderLine> orderLines = toOrderLineConverter.convertFrom(singletonList(MILK), milksAndBread());

        assertThat(orderLines.get(0).getId()).isEqualTo(ProductFactory.MILK.getId());
    }

    @Test
    public void order_line_for_milk_contains_unit_price() {
        final List<OrderLine> orderLines = toOrderLineConverter.convertFrom(singletonList(MILK), milksAndBread());

        assertThat(orderLines.get(0).getPrice()).isEqualTo(ProductFactory.MILK.getPrice());
    }

    @Test
    public void order_line_for_milk_contains_unit() {
        final List<OrderLine> orderLines = toOrderLineConverter.convertFrom(singletonList(MILK), milksAndBread());

        assertThat(orderLines.get(0).getUnit()).isEqualTo(ProductFactory.MILK.getUnit());
    }

    @Test
    public void order_line_for_milk_contains_3_bottles() {
        final List<OrderLine> orderLines = toOrderLineConverter.convertFrom(singletonList(MILK), milksAndBread());

        assertThat(orderLines.get(0).getQuantity()).isEqualTo(3);
    }

    @Test
    public void order_line_should_contain_subtotal() {
        final List<OrderLine> orderLines = toOrderLineConverter.convertFrom(singletonList(MILK), milksAndBread());

        assertThat(orderLines.get(0).getSubtotal()).isEqualTo(valueOf(3.9));
    }

    @Test
    public void order_line_should_contain_total() {
        final List<OrderLine> orderLines = toOrderLineConverter.convertFrom(singletonList(MILK), milksAndBread());

        assertThat(orderLines.get(0).getTotal()).isEqualTo(valueOf(3.9));
    }
}