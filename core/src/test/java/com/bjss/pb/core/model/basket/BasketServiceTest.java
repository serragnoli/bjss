package com.bjss.pb.core.model.basket;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static com.bjss.pb.core.actions.PurchasedProductsBasketFactory.noDiscount;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class BasketServiceTest {

    @Mock
    private ProductIdNormaliser productsNormaliser;

    @Mock
    private BasketValidator validator;

    @InjectMocks
    private BasketService basketService;

    @Test
    public void should_invoke_validator() {
        basketService.normalise(noDiscount());

        verify(validator).validate(Matchers.<Map<String, List<String>>> any());
    }

    @Test
    public void should_invoke_product_normaliser() {
        basketService.normalise(noDiscount());

        verify(productsNormaliser).normalise(anyListOf(String.class));
    }
}