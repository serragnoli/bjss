package com.bjss.pb.core.model.order;

import com.bjss.pb.core.model.promotion.AppliedPromotion;
import com.bjss.pb.core.model.promotion.AppliedPromotionFactory;
import org.junit.Test;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;

public class OrderTest {

    public static final String NAME = "Promotion Name";
    public static final String PRODUCT_ID = "productId";

    @Test
    public void should_calculate_subtotal() {
        final Order order = new Order(OrderLinesFactory.linesWithSubTotalOf12());

        assertThat(order.getSubtotal()).isEqualTo(valueOf(12));
    }

    @Test
    public void should_register_applied_promotion() {
        final Order order = OrderFactory.milkBreadAndApples();

        order.registerPromotion(AppliedPromotionFactory.appliedPromotionTo(NAME, PRODUCT_ID, TEN));

        assertThat(order.hasPromotions()).isTrue();
    }

    @Test
    public void should_register_multiple_applied_promotion() {
        final Order order = OrderFactory.milkBreadAndApples();

        order.registerPromotion(AppliedPromotion.builder().name(NAME).discountableProductId(PRODUCT_ID).discount(TEN).build());
        order.registerPromotion(AppliedPromotion.builder().name("OtherName").discountableProductId(PRODUCT_ID).discount(ONE).build());

        assertThat(order.getAppliedPromotions()).hasSize(2);
    }

    @Test
    public void should_reduce_total_proportional_to_promotion_amount() {
        final Order order = OrderFactory.milkBreadAndApples();

        order.registerPromotion(AppliedPromotion.builder().name(NAME).discountableProductId(PRODUCT_ID).discount(valueOf(0.1)).build());

        assertThat(order.getTotal()).isEqualTo(valueOf(3.0));
    }
}