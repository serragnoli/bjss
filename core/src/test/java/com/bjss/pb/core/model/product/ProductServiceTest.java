package com.bjss.pb.core.model.product;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.bjss.pb.core.model.basket.NormalisedProductFactory.milksAndBread;
import static org.mockito.Matchers.anySetOf;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @Mock
    private ProductRepository repository;

    @InjectMocks
    private ProductService productService;

    @Test
    public void should_invoke_repository() {
        productService.mapFrom(milksAndBread());

        verify(repository).fetchAll(anySetOf(String.class));
    }
}