package com.bjss.pb.core.model.basket;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.bjss.pb.core.actions.TestCommons.APPLES;
import static com.bjss.pb.core.actions.TestCommons.BREAD;
import static com.bjss.pb.core.actions.TestCommons.MILK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

public class ProductIdNormaliserTest {

    private static final List<String> TWO_MILKS_AND_ONE_BREAD = Arrays.asList(MILK, BREAD, MILK);
    private static final List<String> MILKS_2_BREAD_1_APPLE_4 = Arrays.asList(APPLES, MILK, APPLES, BREAD, APPLES, MILK, APPLES);

    private ProductIdNormaliser normaliser;

    @Before
    public void setUp() {
        normaliser = new ProductIdNormaliser();
    }

    @Test
    public void should_normalise_1_product() {
        final Map<String, Integer> normalisedProducts = normaliser.normalise(TWO_MILKS_AND_ONE_BREAD);

        assertThat(normalisedProducts).contains(entry(BREAD, 1));
    }

    @Test
    public void should_normalise_2_products() {
        final Map<String, Integer> normalisedProducts = normaliser.normalise(TWO_MILKS_AND_ONE_BREAD);

        assertThat(normalisedProducts).contains(entry(MILK, 2));
    }

    @Test
    public void shouldnormalise_4_products() {
        final Map<String, Integer> normalisedProducts = normaliser.normalise(MILKS_2_BREAD_1_APPLE_4);

        assertThat(normalisedProducts).contains(entry(APPLES, 4));
    }
}