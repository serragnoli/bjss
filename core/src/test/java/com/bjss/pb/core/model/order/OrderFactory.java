package com.bjss.pb.core.model.order;

import static com.bjss.pb.core.model.product.ProductFactory.APPLES;
import static com.bjss.pb.core.model.product.ProductFactory.BREAD;
import static com.bjss.pb.core.model.product.ProductFactory.MILK;

public class OrderFactory {

    public static Order milkAndBread() {
        return new Order(OrderLinesFactory.linesWithSubTotalOf12());
    }

    public static Order milkBreadAndApples() {
        return new Order(OrderLinesFactory.linesWith(MILK, BREAD, APPLES));
    }

    public static Order milkBreadAnd2Soups() {
        return new Order(OrderLinesFactory.linesWithBreadAnd2TinsOfSoup());
    }
}
