package com.bjss.pb.core.infrastructure.repositories;

import com.bjss.pb.core.model.product.Product;
import com.bjss.pb.core.model.product.Unit;
import com.bjss.pb.core.model.promotion.B2gohpDiscounter;
import com.bjss.pb.core.model.promotion.Discounter;
import com.bjss.pb.core.model.promotion.TenPercentDiscounter;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataStore {

    public static final Map<String, Product> PRODUCTS;
    public static final List<Discounter> DISCOUNTERS;

    static {
        PRODUCTS = new HashMap<>();

        PRODUCTS.put("Soup", new Product("Soup", Unit.TIN, BigDecimal.valueOf(0.65)));
        PRODUCTS.put("Bread", new Product("Bread", Unit.LOAF, BigDecimal.valueOf(0.8)));
        PRODUCTS.put("Milk", new Product("Milk", Unit.BOTTLE, BigDecimal.valueOf(1.3)));
        PRODUCTS.put("Apples", new Product("Apples", Unit.BAG, BigDecimal.valueOf(1)));

        DISCOUNTERS = Arrays.asList(
                new TenPercentDiscounter(Collections.singletonList(PRODUCTS.get("Apples"))),
                new B2gohpDiscounter(Collections.singletonMap(PRODUCTS.get("Soup"),PRODUCTS.get("Bread")))
        );
    }

}
