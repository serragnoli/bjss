package com.bjss.pb.core.model.product;

import java.util.List;
import java.util.Map;

public class ProductService {

    private final ProductRepository repository;

    public ProductService(final ProductRepository repository) {
        this.repository = repository;
    }

    public List<Product> mapFrom(final Map<String, Integer> normalisedProductIds) {
        return repository.fetchAll(normalisedProductIds.keySet());
    }
}
