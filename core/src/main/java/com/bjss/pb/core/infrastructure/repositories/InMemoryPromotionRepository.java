package com.bjss.pb.core.infrastructure.repositories;

import com.bjss.pb.core.model.promotion.Discounter;
import com.bjss.pb.core.model.promotion.PromotionRepository;

import java.util.List;

public class InMemoryPromotionRepository implements PromotionRepository {

    @Override
    public List<Discounter> fetchAllDiscounters() {
        return DataStore.DISCOUNTERS;
    }
}
