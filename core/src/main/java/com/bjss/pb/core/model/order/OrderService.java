package com.bjss.pb.core.model.order;

import com.bjss.pb.core.model.product.Product;

import java.util.List;
import java.util.Map;

public class OrderService {

    private final ToOrderLineConverter toOrderLineConverter;

    public OrderService(final ToOrderLineConverter toOrderLineConverter) {
        this.toOrderLineConverter = toOrderLineConverter;
    }

    public Order createOrderFrom(final List<Product> products, final Map<String, Integer> normalisedProdutcIds) {
        final List<OrderLine> orderLines = toOrderLineConverter.convertFrom(products, normalisedProdutcIds);

        return new Order(orderLines);
    }
}
