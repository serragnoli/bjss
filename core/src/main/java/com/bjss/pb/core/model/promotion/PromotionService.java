package com.bjss.pb.core.model.promotion;

import com.bjss.pb.core.model.order.Order;

import java.util.List;

public class PromotionService {

    private final PromotionRepository repository;

    public PromotionService(final PromotionRepository repository) {
        this.repository = repository;
    }

    public Order applyPromotionsFor(final Order order) {
        final List<Discounter> discounters = repository.fetchAllDiscounters();

        discounters.forEach(d -> d.applyDiscount(order));

        return order;
    }
}
