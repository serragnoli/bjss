package com.bjss.pb.core.model.promotion;

import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.order.OrderLine;
import com.bjss.pb.core.model.product.Product;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static java.math.BigDecimal.valueOf;

@EqualsAndHashCode
public class B2gohpDiscounter implements Discounter {

    static final String NAME = "2 %s(s) of %s give 50%% off %s: -";

    private static final BigDecimal DISCOUNT = valueOf(0.5);
    private static final int ELIGIBILITY_MIN_QTY = 2;

    private final Map<Product, Product> triggerReward;

    public B2gohpDiscounter(final Map<Product, Product> triggerReward) {
        this.triggerReward = triggerReward;
    }

    @Override
    public void applyDiscount(final Order order) {

        final Map<Product, Product> discountableAsKey = retrieveDiscountableProducts(order);

        final Map<OrderLine, Product> discountableLines = findEligibleOrderLine(order, discountableAsKey);


        discountableLines.entrySet().forEach(entry -> {
            final Product triggerProduct = entry.getValue();
            final OrderLine eligibleOrderLine = entry.getKey();
            final BigDecimal discount = eligibleOrderLine.getTotal().multiply(DISCOUNT);
            final String formattedName = String.format(NAME, triggerProduct.getUnit().getDisplay(),
                    triggerProduct.getId(), eligibleOrderLine.getId());

            final AppliedPromotion appliedPromotion = AppliedPromotion.builder()
                    .name(formattedName)
                    .unit(triggerProduct.getUnit().getDisplay())
                    .triggerProductId(triggerProduct.getId())
                    .discountableProductId(eligibleOrderLine.getId())
                    .discount(discount)
                    .build();

            order.registerPromotion(appliedPromotion);
        });
    }

    private Map<OrderLine, Product> findEligibleOrderLine(final Order order, final Map<Product, Product> discountableAsKey) {
        final Map<OrderLine, Product> eligibleOrderLine = new HashMap<>();
        order.getItems().forEach(ol -> discountableAsKey.entrySet().forEach(p -> {
            if (ol.getId().equalsIgnoreCase(p.getKey().getId())) {
                eligibleOrderLine.put(ol, p.getValue());
            }
        }));
        return eligibleOrderLine;
    }

    private Map<Product, Product> retrieveDiscountableProducts(final Order order) {
        final Map<Product, Product> discountableProductAsKey = new HashMap<>();
        order.getItems().forEach(ol -> triggerReward.entrySet().forEach(p -> {
            if (ol.getId().equalsIgnoreCase(p.getKey().getId()) && ol.getQuantity() >= ELIGIBILITY_MIN_QTY) {
                discountableProductAsKey.put(p.getValue(), p.getKey());
            }
        }));

        return discountableProductAsKey;
    }
}
