package com.bjss.pb.core.model.order;

import com.bjss.pb.core.model.product.Unit;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;

@Getter
@ToString
public class OrderLine {
    private final String id;
    private final BigDecimal price;
    private final Unit unit;
    private final Integer quantity;
    private final BigDecimal discount;
    private final BigDecimal subtotal;
    private final BigDecimal total;

    public OrderLine(final String id, final BigDecimal price, final Unit unit, final Integer quantity) {
        this.id = id;
        this.price = price;
        this.unit = unit;
        this.quantity = quantity;
        this.discount = ZERO;
        this.subtotal = price.multiply(valueOf(quantity));
        this.total = subtotal;
    }
}
