package com.bjss.pb.core.model.order;

import com.bjss.pb.core.model.product.Product;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.math.BigDecimal.valueOf;

public class ToOrderLineConverter {

    public List<OrderLine> convertFrom(final List<Product> products, final Map<String, Integer> normalisedProductIds) {
        return products.stream()
                .map(convertToOrderItem(normalisedProductIds))
                .collect(Collectors.<OrderLine>toList());
    }

    private Function<Product, OrderLine> convertToOrderItem(final Map<String, Integer> normalisedProductIds) {
        return product -> {
            final Integer quantity = normalisedProductIds.get(product.getId());

            return new OrderLine(product.getId(), product.getPrice(), product.getUnit(), quantity);
        };
    }
}
