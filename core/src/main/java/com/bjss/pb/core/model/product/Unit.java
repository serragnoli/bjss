package com.bjss.pb.core.model.product;

import lombok.Getter;

@Getter
public enum Unit {
    TIN("Tin"), LOAF("Loaf"), BOTTLE("Bottle"), BAG("Bag");

    private final String display;

    Unit(final String display) {
        this.display = display;
    }
}
