package com.bjss.pb.core.model.basket;

import com.bjss.pb.core.model.basket.exceptions.NoProductInListException;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Map;

import static com.bjss.pb.core.infrastructure.utils.ActionsCommonParams.BASKET_PARAM;

public class BasketValidator {

    public void validate(final Map<String, List<String>> basketParam) {
        final List<String> productsList = basketParam.get(BASKET_PARAM);

        if (CollectionUtils.isEmpty(productsList)) {
            throw new NoProductInListException();
        }
    }
}
