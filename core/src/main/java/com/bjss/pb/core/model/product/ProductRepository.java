package com.bjss.pb.core.model.product;

import java.util.List;
import java.util.Set;

public interface ProductRepository {

    List<Product> fetchAll(Set<String> productIds);
}
