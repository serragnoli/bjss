package com.bjss.pb.core.infrastructure.repositories;

import com.bjss.pb.core.model.product.Product;
import com.bjss.pb.core.model.product.ProductRepository;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class InMemoryProductRepository implements ProductRepository {

    @Override
    public List<Product> fetchAll(final Set<String> productIds) {
        return DataStore.PRODUCTS.entrySet().stream()
                .filter(p -> productIds.contains(p.getKey()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }
}
