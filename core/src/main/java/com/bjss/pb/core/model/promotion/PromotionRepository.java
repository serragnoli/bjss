package com.bjss.pb.core.model.promotion;

import java.util.List;

public interface PromotionRepository {

    List<Discounter> fetchAllDiscounters();
}
