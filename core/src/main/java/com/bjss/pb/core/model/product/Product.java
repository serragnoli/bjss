package com.bjss.pb.core.model.product;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@EqualsAndHashCode
public class Product {

    private final String id;
    private final Unit unit;
    private final BigDecimal price;

    public Product(final String id, final Unit unit, final BigDecimal price) {
        this.id = id;
        this.unit = unit;
        this.price = price;
    }
}
