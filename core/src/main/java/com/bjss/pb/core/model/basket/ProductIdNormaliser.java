package com.bjss.pb.core.model.basket;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class ProductIdNormaliser {

    public Map<String, Integer> normalise(final List<String> productIds) {
        return productIds.stream()
                .collect(groupingBy(Function.identity(), summingInt(e -> 1)));
    }
}
