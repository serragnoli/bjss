package com.bjss.pb.core.model.promotion;

import com.bjss.pb.core.model.order.Order;

public interface Discounter {
    void applyDiscount(Order order);
}
