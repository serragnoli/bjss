package com.bjss.pb.core.model.order;

import com.bjss.pb.core.model.promotion.AppliedPromotion;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.math.BigDecimal.ZERO;

@Getter
@ToString
public class Order {

    private BigDecimal total;
    private BigDecimal subtotal;
    private final List<OrderLine> items;
    private List<AppliedPromotion> appliedPromotions;

    public Order(final List<OrderLine> items) {
        this.items = items;
        appliedPromotions = new ArrayList<>();

        calculateSubtotal(items);
        calculateTotal(items);
    }

    public void calculateSubtotal(final List<OrderLine> orderLines) {
        subtotal = orderLines.stream()
                .map(OrderLine::getSubtotal)
                .reduce(ZERO, BigDecimal::add);
    }

    private void calculateTotal(List<OrderLine> orderLines) {
        total = orderLines.stream()
                .map(OrderLine::getTotal)
                .reduce(ZERO, BigDecimal::add);
    }

    public boolean hasPromotions() {
        return CollectionUtils.isNotEmpty(appliedPromotions);
    }

    public void registerPromotion(final AppliedPromotion applied) {
        this.appliedPromotions.add(applied);

        this.total = this.total.subtract(applied.getDiscount());
    }
}
