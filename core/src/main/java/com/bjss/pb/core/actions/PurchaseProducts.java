package com.bjss.pb.core.actions;

import com.bjss.pb.core.model.basket.BasketService;
import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.order.OrderService;
import com.bjss.pb.core.model.product.Product;
import com.bjss.pb.core.model.product.ProductService;
import com.bjss.pb.core.model.promotion.PromotionService;

import java.util.List;
import java.util.Map;

public class PurchaseProducts {

    private final BasketService basketService;
    private final ProductService productService;
    private final OrderService orderService;
    private final PromotionService promotionService;

    public PurchaseProducts(final BasketService basketService, final ProductService productService,
                            final OrderService orderService, final PromotionService promotionService) {
        this.basketService = basketService;
        this.productService = productService;
        this.orderService = orderService;
        this.promotionService = promotionService;
    }

    public Order purchase(final Map<String, List<String>> basketParam) {
        final Map<String, Integer> normalisedProdIds = basketService.normalise(basketParam);

        final List<Product> products = productService.mapFrom(normalisedProdIds);

        final Order order = orderService.createOrderFrom(products, normalisedProdIds);

        return promotionService.applyPromotionsFor(order);
    }
}
