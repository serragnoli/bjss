package com.bjss.pb.core.model.promotion;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Builder
@ToString
@EqualsAndHashCode
@AllArgsConstructor(access = PRIVATE)
public class AppliedPromotion {

    private String name;
    private String discountableProductId;
    private String unit;
    private String triggerProductId;
    private BigDecimal discount;

}
