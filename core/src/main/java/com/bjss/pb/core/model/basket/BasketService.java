package com.bjss.pb.core.model.basket;

import java.util.List;
import java.util.Map;

import static com.bjss.pb.core.infrastructure.utils.ActionsCommonParams.BASKET_PARAM;

public class BasketService {

    private final ProductIdNormaliser productIdNormaliser;
    private final BasketValidator validator;

    public BasketService( final BasketValidator validator, final ProductIdNormaliser productIdNormaliser) {
        this.validator = validator;
        this.productIdNormaliser = productIdNormaliser;
    }

    public Map<String, Integer> normalise(final Map<String, List<String>> productIdsParam) {
        validator.validate(productIdsParam);

        return productIdNormaliser.normalise(productIdsParam.get(BASKET_PARAM));
    }
}
