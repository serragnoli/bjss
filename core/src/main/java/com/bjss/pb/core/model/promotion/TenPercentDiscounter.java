package com.bjss.pb.core.model.promotion;

import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.order.OrderLine;
import com.bjss.pb.core.model.product.Product;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.math.BigDecimal.valueOf;
import static lombok.AccessLevel.PUBLIC;

@EqualsAndHashCode
@AllArgsConstructor(access = PUBLIC)
public class TenPercentDiscounter implements Discounter {

    static final String NAME = "%s 10%% off: -";
    private static final BigDecimal DISCOUNT = valueOf(0.1);

    private final List<Product> applicableProducts;

    @Override
    public void applyDiscount(final Order order) {

        final List<OrderLine> applicableLines = new ArrayList<>();
        order.getItems().forEach(ol -> applicableProducts.forEach(p -> {
            if (ol.getId().equalsIgnoreCase(p.getId())) {
                applicableLines.add(ol);
            }
        }));

        applicableLines.forEach(orderLine -> {
            final String formattedName = String.format(NAME, orderLine.getId());
            final AppliedPromotion appliedPromotion = AppliedPromotion.builder()
                    .name(formattedName)
                    .discountableProductId(orderLine.getId())
                    .discount(orderLine.getTotal().multiply(DISCOUNT))
                    .build();

            order.registerPromotion(appliedPromotion);
        });
    }
}
