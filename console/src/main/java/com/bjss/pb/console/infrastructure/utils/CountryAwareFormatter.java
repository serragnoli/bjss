package com.bjss.pb.console.infrastructure.utils;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class CountryAwareFormatter {

    private static final int NEGATIVE = -1;

    public String format(final BigDecimal amount) {
        if(amount.compareTo(BigDecimal.ONE) == NEGATIVE) {
            final BigDecimal pence = amount.multiply(BigDecimal.valueOf(100)).setScale(0, ROUND_HALF_UP);

            return pence.toPlainString() + "p";
        }

        final BigDecimal pounds = amount.setScale(2, ROUND_HALF_UP);

        return "£" + pounds.toPlainString();
    }
}
