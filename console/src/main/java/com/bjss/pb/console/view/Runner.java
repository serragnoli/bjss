package com.bjss.pb.console.view;

import com.bjss.pb.console.infrastructure.CommandPromptConsole;
import com.bjss.pb.console.infrastructure.utils.CountryAwareFormatter;
import com.bjss.pb.core.actions.PurchaseProducts;
import com.bjss.pb.core.infrastructure.repositories.InMemoryProductRepository;
import com.bjss.pb.core.infrastructure.repositories.InMemoryPromotionRepository;
import com.bjss.pb.core.model.basket.BasketService;
import com.bjss.pb.core.model.basket.BasketValidator;
import com.bjss.pb.core.model.basket.ProductIdNormaliser;
import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.order.OrderService;
import com.bjss.pb.core.model.order.ToOrderLineConverter;
import com.bjss.pb.core.model.product.ProductRepository;
import com.bjss.pb.core.model.product.ProductService;
import com.bjss.pb.core.model.promotion.PromotionRepository;
import com.bjss.pb.core.model.promotion.PromotionService;

import java.util.Scanner;

public class Runner {
    private static final String INPUT_MESSAGE = "Enter your order:";

    public static void main(String[] args) {
        final Pos pos = buildDependencies();

        inputCollector(pos);
    }

    private static Pos buildDependencies() {
        final BasketValidator validator = new BasketValidator();
        final ProductIdNormaliser productIdNormaliser = new ProductIdNormaliser();
        final BasketService basketService = new BasketService(validator, productIdNormaliser);
        final ProductRepository productRepository = new InMemoryProductRepository();
        final ProductService productService = new ProductService(productRepository);
        final ToOrderLineConverter toOrderLineConverter = new ToOrderLineConverter();
        final OrderService orderService = new OrderService(toOrderLineConverter);
        final PromotionRepository promotionRepository = new InMemoryPromotionRepository();
        final PromotionService promotionService = new PromotionService(promotionRepository);
        final PurchaseProducts purchaseProducts = new PurchaseProducts(basketService, productService, orderService, promotionService);

        return new Pos(purchaseProducts);
    }

    private static void inputCollector(final Pos pos) {
        System.out.print(INPUT_MESSAGE);
        Scanner input = new Scanner(System.in);

        String text;
        while ((text = input.nextLine()) != null) {
            final Order order = pos.placeOrderFor(text.trim().split(" "));

            final CountryAwareFormatter countryAwareFormatter = new CountryAwareFormatter();
            final Console console = new CommandPromptConsole();
            final OrderPrinter orderPrinter = new OrderPrinter(order, countryAwareFormatter, console);

            orderPrinter.print();

            System.out.print(INPUT_MESSAGE);
        }
    }
}
