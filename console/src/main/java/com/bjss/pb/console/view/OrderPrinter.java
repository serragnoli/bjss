package com.bjss.pb.console.view;

import com.bjss.pb.console.infrastructure.utils.CountryAwareFormatter;
import com.bjss.pb.core.model.order.Order;

import java.math.BigDecimal;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class OrderPrinter {

    private final Order order;
    private final CountryAwareFormatter countryAwareFormatter;
    private final Console console;

    public OrderPrinter(final Order order, final CountryAwareFormatter countryAwareFormatter, Console console) {
        this.order = order;
        this.countryAwareFormatter = countryAwareFormatter;
        this.console = console;
    }

    public String printSubtotal() {
        return String.format("Subtotal: £%s\n", scale(order.getSubtotal()));
    }

    public String printPromotions() {
        if (order.hasPromotions()) {
            return order.getAppliedPromotions().stream()
                    .map(p -> p.getName() + countryAwareFormatter.format(p.getDiscount()) + "\n")
                    .collect(Collectors.joining(""));
        }

        return "(No offers available)\n";
    }

    public String printTotal() {
        return String.format("Total price: £%S", scale(order.getTotal()));
    }

    private String scale(final BigDecimal amount) {
        return amount.setScale(2, ROUND_HALF_UP).toPlainString();
    }

    public void print() {
        console.print(
                printSubtotal() + printPromotions() + printTotal()
        );
    }
}
