package com.bjss.pb.console.infrastructure;

import com.bjss.pb.console.view.Console;

public class CommandPromptConsole implements Console {

    @Override
    public void print(final String content) {
        System.out.println(content);
        System.out.println("\n==================================================================\n");
    }
}
