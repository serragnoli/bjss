package com.bjss.pb.console.view;

import com.bjss.pb.core.actions.PurchaseProducts;
import com.bjss.pb.core.model.order.Order;

import java.util.List;
import java.util.Map;

import static com.bjss.pb.core.infrastructure.utils.ActionsCommonParams.BASKET_PARAM;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonMap;

public class Pos {

    private final PurchaseProducts purchaseProducts;

    public Pos(final PurchaseProducts purchaseProducts) {
        this.purchaseProducts = purchaseProducts;
    }

    public Order placeOrderFor(final String... products) {
        final Map<String, List<String>> shoppingListParam = singletonMap(BASKET_PARAM, asList(products));

        return purchaseProducts.purchase(shoppingListParam);

    }
}
