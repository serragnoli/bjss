package com.bjss.pb.console;

import com.bjss.pb.core.model.product.Product;
import com.bjss.pb.core.model.product.Unit;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;

public class ProductFactory {
    public static final Product MILK = new Product("MILK", Unit.BOTTLE, valueOf(1.3));
    public static final Product BREAD = new Product("BREAD", Unit.LOAF, valueOf(0.8));
    public static final Product APPLES = new Product("APPLES", Unit.BAG, valueOf(1.0));
}
