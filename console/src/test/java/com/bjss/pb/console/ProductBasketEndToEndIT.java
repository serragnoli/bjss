package com.bjss.pb.console;

import com.bjss.pb.console.infrastructure.utils.CountryAwareFormatter;
import com.bjss.pb.console.view.Console;
import com.bjss.pb.console.view.OrderPrinter;
import com.bjss.pb.console.view.Pos;
import com.bjss.pb.core.actions.PurchaseProducts;
import com.bjss.pb.core.infrastructure.repositories.InMemoryProductRepository;
import com.bjss.pb.core.infrastructure.repositories.InMemoryPromotionRepository;
import com.bjss.pb.core.model.basket.BasketService;
import com.bjss.pb.core.model.basket.BasketValidator;
import com.bjss.pb.core.model.basket.ProductIdNormaliser;
import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.order.OrderService;
import com.bjss.pb.core.model.order.ToOrderLineConverter;
import com.bjss.pb.core.model.product.ProductRepository;
import com.bjss.pb.core.model.product.ProductService;
import com.bjss.pb.core.model.promotion.PromotionRepository;
import com.bjss.pb.core.model.promotion.PromotionService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static com.bjss.pb.console.TestCommons.APPLES;
import static com.bjss.pb.console.TestCommons.BREAD;
import static com.bjss.pb.console.TestCommons.MILK;
import static com.bjss.pb.console.TestCommons.SOUP;
import static org.assertj.core.api.Assertions.assertThat;

public class ProductBasketEndToEndIT {

    private Console console;
    private CountryAwareFormatter countryAwareFormatter;

    private Pos pos;

    @Before
    public void setUp() {
        console = Mockito.mock(Console.class);

        final ProductIdNormaliser productIdNormaliser = new ProductIdNormaliser();
        final BasketValidator validator = new BasketValidator();
        final BasketService basketService = new BasketService(validator, productIdNormaliser);
        final ProductRepository productRepository = new InMemoryProductRepository();
        final ProductService productService = new ProductService(productRepository);
        final ToOrderLineConverter toOrderLineConverter = new ToOrderLineConverter();
        final OrderService orderService = new OrderService(toOrderLineConverter);
        final PromotionRepository promotionRepository = new InMemoryPromotionRepository();
        final PromotionService promotionService = new PromotionService(promotionRepository);

        final PurchaseProducts purchaseProducts = new PurchaseProducts(basketService, productService, orderService, promotionService);

        countryAwareFormatter = new CountryAwareFormatter();
        pos = new Pos(purchaseProducts);
    }

    @Test
    public void should_print_basket_with_no_special_offers() {
        final Order order = pos.placeOrderFor(MILK, BREAD);

        final OrderPrinter orderPrinter = new OrderPrinter(order, countryAwareFormatter, console);

        assertThat(orderPrinter.printSubtotal()).isEqualTo("Subtotal: £2.10\n");
        assertThat(orderPrinter.printPromotions()).isEqualTo("(No offers available)\n");
        assertThat(orderPrinter.printTotal()).isEqualTo("Total price: £2.10");
    }

    @Test
    public void should_print_basket_with_10_percent_discount() {
        final Order order = pos.placeOrderFor(MILK, BREAD, APPLES);

        final OrderPrinter orderPrinter = new OrderPrinter(order, countryAwareFormatter, console);

        assertThat(orderPrinter.printSubtotal()).isEqualTo("Subtotal: £3.10\n");
        assertThat(orderPrinter.printPromotions()).isEqualTo("Apples 10% off: -10p\n");
        assertThat(orderPrinter.printTotal()).isEqualTo("Total price: £3.00");
    }

    @Test
    public void should_print_basket_with_50_percent_discount_when_buying_2_soups() {
        final Order order = pos.placeOrderFor(MILK, SOUP, BREAD, SOUP);

        final OrderPrinter orderPrinter = new OrderPrinter(order, countryAwareFormatter, console);

        assertThat(orderPrinter.printSubtotal()).isEqualTo("Subtotal: £3.40\n");
        assertThat(orderPrinter.printPromotions()).isEqualTo("2 Tin(s) of Soup give 50% off Bread: -40p\n");
        assertThat(orderPrinter.printTotal()).isEqualTo("Total price: £3.00");
    }

    @Test
    public void should_print_basket_with_50_percent_of_total_items() {
        final Order order = pos.placeOrderFor(MILK, SOUP, BREAD, SOUP, BREAD, SOUP, SOUP);

        final OrderPrinter orderPrinter = new OrderPrinter(order, countryAwareFormatter, console);

        assertThat(orderPrinter.printSubtotal()).isEqualTo("Subtotal: £5.50\n");
        assertThat(orderPrinter.printPromotions()).isEqualTo("2 Tin(s) of Soup give 50% off Bread: -80p\n");
        assertThat(orderPrinter.printTotal()).isEqualTo("Total price: £4.70");
    }

    @Test
    public void should_print_ignoring_dodgy_product() {
        final Order order = pos.placeOrderFor(MILK, SOUP, BREAD, "Dodgy");

        final OrderPrinter orderPrinter = new OrderPrinter(order, countryAwareFormatter, console);

        assertThat(orderPrinter.printSubtotal()).isEqualTo("Subtotal: £2.75\n");
        assertThat(orderPrinter.printPromotions()).isEqualTo("(No offers available)\n");
        assertThat(orderPrinter.printTotal()).isEqualTo("Total price: £2.75");
    }

    @Test
    public void should_print_2_promotions() {
        final Order order = pos.placeOrderFor(MILK, SOUP, BREAD, SOUP, APPLES);

        final OrderPrinter orderPrinter = new OrderPrinter(order, countryAwareFormatter, console);

        assertThat(orderPrinter.printSubtotal()).isEqualTo("Subtotal: £4.40\n");
        assertThat(orderPrinter.printPromotions()).isEqualTo("Apples 10% off: -10p\n" +
                "2 Tin(s) of Soup give 50% off Bread: -40p\n");
        assertThat(orderPrinter.printTotal()).isEqualTo("Total price: £3.90");
    }
}
