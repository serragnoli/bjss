package com.bjss.pb.console.view;

import com.bjss.pb.console.infrastructure.utils.CountryAwareFormatter;
import com.bjss.pb.core.model.order.Order;
import com.bjss.pb.core.model.promotion.AppliedPromotion;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;

import static java.math.BigDecimal.ZERO;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderPrinterTest {

    @Mock
    private CountryAwareFormatter countryAwareFormatter;

    @Mock
    private Order order;

    @Mock
    private Console console;

    @InjectMocks
    private OrderPrinter orderPrinter;

    @Test
    public void should_invoke_country_formatter() {
        when(order.hasPromotions()).thenReturn(true);
        when(order.getAppliedPromotions()).thenReturn(Collections.singletonList(AppliedPromotion.builder().build()));

        orderPrinter.printPromotions();

        verify(countryAwareFormatter).format(Matchers.any(BigDecimal.class));
    }

    @Test
    public void should_invoke_console() {
        when(order.getSubtotal()).thenReturn(ZERO);
        when(order.getTotal()).thenReturn(ZERO);

        orderPrinter.print();

        verify(console).print(Matchers.anyString());
    }
}