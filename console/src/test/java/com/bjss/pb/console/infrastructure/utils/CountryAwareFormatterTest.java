package com.bjss.pb.console.infrastructure.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CountryAwareFormatterTest {

    @InjectMocks
    private CountryAwareFormatter formatter;

    @Test
    public void should_print_pence_when_amount_less_than_1_pound() {
        final String result = formatter.format(valueOf(0.1));

        assertThat(result).isEqualTo("10p");
    }

    @Test
    public void should_print_pounds_when_amount_1_or_greater() {
        final String result = formatter.format(ONE);

        assertThat(result).isEqualTo("£1.00");
    }
}