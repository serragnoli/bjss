# Tech Stack
* Java 8 (1.8.0_66)
* Maven 3 (3.3.3)

# Running the app
## From the IDE
1. Run the main method in `com.bjss.pb.console.view.Runner`
## From the command line
1. Inside the module **CONSOLE**, run `mvn clean compile assembly:single`
2. Execute the app `java -jar target/PriceBasketConsole-1.0.0-SNAPSHOT-jar-with-dependencies.jar`

# Requirement:
* Write a program and associated unit tests that can price a basket of goods taking into account some special offers.
+ The goods that can be purchased, together with their normal prices are:
    * Soup – 65p per tin
    * Bread – 80p per loaf
    * Milk – £1.30 per bottle
    * Apples – £1.00 per bag    
+ Current special offers:
    * Apples have a 10% discount off their normal price this week
    * Buy 2 tins of soup and get a loaf of bread for half price    
* Output should be to the console, for example:
```
Subtotal: £3.10
Apples 10% off: -10p
Total: £3.00
```

* If no special offers are applicable the code should output:
```
Subtotal: £1.30
(No offers available)
Total price: £1.30
```

# Structure of the project:
## Delivery strategy
### console
 * This is the delivery mechanism for this domain.

## Domain
### core
 * Contains the domain of the application.
 * This is supposed to be reusable for different delivery mechanisms i.e. Rest, Web etc.

### core/actions
 * All the system can do is documented by classes at this level.
 * This is effectively the entry point of the application

### core/model
 * This contains the concepts of this domain
 * I have identified only think of two domain concepts for this application: TIME and LIGHTS
 
### core/infrastructure
 * This contains the technology-specific classes like the Repository implementations
